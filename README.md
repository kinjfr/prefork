
[![Docs](https://img.shields.io/docsrs/prefork/latest)](https://docs.rs/prefork/latest/prefork/)

# Prefork

Fork a process in Rust to create multiple child processes.

This repository contains a Rust crate. See [the example code](examples/axum.rs)
which opens a socket and then forks multiple processes that accept connections on that socket. This is an old fashioned way of multiprocessing, but it is sometimes useful when dealing with systems that run poorly when multi-threaded (e.g. when embedding a Python interpreter).

