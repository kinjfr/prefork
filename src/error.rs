use nix::errno::Errno;
use thiserror::Error;

/// The result of attempting to fork.
pub type Result<T> = std::result::Result<T, ForkErr>;

/// Possible errors that can occur when attempting to fork.
#[derive(Error, Debug)]
pub enum ForkErr {
    /// There was an error calling fork().
    #[error("could not fork process")]
    Fork(#[from] Errno),
    /// There was an error registering the signal handlers in the parent process.
    #[error("could not register signal handlers")]
    SignalHandler(#[from] std::io::Error),
}
